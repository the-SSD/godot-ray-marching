@tool
extends EditorPlugin

var ray_marching_widow = preload("res://addons/ray_marching/window.tscn")

var editor_cameras: Array = []
var instances: Array[MeshInstance3D] = []

func _enter_tree():
	
	find_cameras(get_parent()) # this is a work around
	for camera in editor_cameras:
		var window_inst = ray_marching_widow.instantiate()
		instances.append(window_inst)
		camera.add_child(window_inst)


func _exit_tree():
	for inst in instances:
		inst.queue_free()

func find_cameras(n : Node):
	if n is Camera3D:
		editor_cameras.append(n)
		return
		
	for c in n.get_children():
		find_cameras(c)
