# Godot Ray Marching

## Description
Ray Marching for Godot 4
thing that are working:
1. Materials
2. Reflection
3. Mixing with normal rendering (both can be renderd at the same time with correct depth)

## Usage
Edit shader/Scene most of thing are there
of shader/Color
if you want to change constants then change addons/ray marching/shaders/consts

## License
MIT

## Project status
Not being worked on
1. Godot frezes for a split second when editing shaders
2. seams to be slower then should be and Godot shaders are not ment to be usesed like this
3. I will work on glsl version insted for now
4. Godot doesn't use glsl shaders for material(this is a good idea but I can't implement some optimasations I have in mind)
to use glsl you need to overide the Godot renderer (whitch doesn't work yet)